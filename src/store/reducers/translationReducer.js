import {
    ACTION_TRANSLATION_ERROR,
    ACTION_TRANSLATION_SAVE,
    ACTION_TRANSLATION_SUCCESS
} from "../actions/translationActions";

const initialState = {
    translationText: "",
    signArray: [],
    showTranslation: false,
    TranslationError: "",
    savingTranslation: false
}

export const translationReducer = (state = {...initialState}, action) => {
    switch (action.type) {
        case ACTION_TRANSLATION_SAVE:
            return {
                ...state,
                signArray: [],
                translationError: '',
                savingTranslation: true
            }
        case ACTION_TRANSLATION_SUCCESS:
            return {
                ...state,
                signArray: action.payload.signs,
                showTranslation: true,
                savingTranslation: false,
            }
        case ACTION_TRANSLATION_ERROR:
            return {
                ...state,
                translationError: action.payload.signs
            }
        default:
            return state;
    }
}