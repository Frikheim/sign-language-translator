import {ACTION_TRANSLATION_SAVE, translationErrorAction, translationSuccessAction} from "../actions/translationActions";
import {TranslatorAPI} from "../../components/Translator/TranslatorAPI";

export const translationMiddleware = ({ dispatch }) => next => action => {
    next(action)

    if (action.type === ACTION_TRANSLATION_SAVE) {

        TranslatorAPI.saveTranslation(action.payload)
            .then(translation => {
                dispatch(translationSuccessAction(translation));
            })
            .catch(({ message }) => {
                dispatch(translationErrorAction(message));

            })

    }

}
