import {
    ACTION_LOGIN_ATTEMPTING, ACTION_LOGIN_NEW_USER,
    ACTION_LOGIN_SUCCESS, ACTION_LOGIN_CHECK_USER,
    loginErrorAction, loginCheckUserAction,
    loginSuccessAction, loginNewUserAction
} from "../actions/loginActions";
import {LoginAPI} from "../../components/Login/LoginAPI";
import {sessionSetAction} from "../actions/sessionActions";

export const loginMiddleware = ({dispatch}) => next => action => {
    next(action);

    if (action.type === ACTION_LOGIN_ATTEMPTING) {
        LoginAPI.login()
            .then(names => {
                let check = [action.payload,names];
                dispatch(loginCheckUserAction(check));
            })
            .catch(error => {
                dispatch(loginErrorAction(error.message));
            })
    }
    if (action.type === ACTION_LOGIN_NEW_USER) {
        LoginAPI.newUser(action.payload)
            .then(user => {
                dispatch(loginSuccessAction(user));
            })
            .catch(error => {
                dispatch(loginErrorAction(error.message));
            })
    }

    if (action.type === ACTION_LOGIN_SUCCESS) {
        dispatch(sessionSetAction(action.payload))
    }

    if (action.type === ACTION_LOGIN_CHECK_USER) {
        let names = action.payload[1];
        let user = action.payload[0];
        let userFound = false;
        for (let i = 0;i <names.length;i++) {
            if (names[i].name === user.name ) {
                userFound = true;
                user = names[i];
            }
        }
        if (userFound) {
            dispatch(loginSuccessAction(user))
        }
        else {
            dispatch(loginNewUserAction(user));
        }
    }
}