export const ACTION_LOGIN_ATTEMPTING = '[login] ATTEMPT'
export const ACTION_LOGIN_NEW_USER = '[login] NEW_USER'
export const ACTION_LOGIN_CHECK_USER = '[login] CHECK_USER'
export const ACTION_LOGIN_SUCCESS = '[login] SUCCESS'
export const ACTION_LOGIN_ERROR = '[login] ERROR'

export const loginAttemptAction = name => ({
    type: ACTION_LOGIN_ATTEMPTING,
    payload: name
})

export const loginCheckUserAction = check => ({
    type: ACTION_LOGIN_CHECK_USER,
    payload: check
})

export const loginNewUserAction = user => ({
    type: ACTION_LOGIN_NEW_USER,
    payload: user
})

export const loginSuccessAction = name => ({
    type: ACTION_LOGIN_SUCCESS,
    payload: name
})

export const loginErrorAction = error => ({
    type: ACTION_LOGIN_ERROR,
    payload: error
})