export const ACTION_TRANSLATION_SAVE = "[translation] SAVE"
export const ACTION_TRANSLATION_SUCCESS = "[translation] SUCCESS"
export const ACTION_TRANSLATION_ERROR = "[translation] ERROR"

export const translationSaveAction = translation => ({
    type: ACTION_TRANSLATION_SAVE,
    payload: translation
})

export const translationSuccessAction = translation => ({
    type: ACTION_TRANSLATION_SUCCESS,
    payload: translation
})

export const translationErrorAction = error => ({
    type: ACTION_TRANSLATION_ERROR,
    payload: error
})


