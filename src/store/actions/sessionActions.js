export const ACTION_SESSION_SET = "[session] SET";
export const ACTION_SESSION_INIT = "[session] INIT";
export const ACTION_SESSION_LOGOUT = "[session] LOGOUT";

export const sessionSetAction = name => ({
    type: ACTION_SESSION_SET,
    payload: name
})

export const sessionInitAction = () => ({
    type: ACTION_SESSION_INIT
})

export const sessionLogoutAction = () => ({
    type: ACTION_SESSION_LOGOUT
})