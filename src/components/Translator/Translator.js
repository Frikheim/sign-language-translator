import {Link, Redirect} from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";
import {useDispatch, useSelector} from "react-redux";
import {useState} from "react";
import { translationSaveAction} from "../../store/actions/translationActions";

const Translator = () => {

    const dispath = useDispatch();
    const {loggedIn} = useSelector(state => state.sessionReducer);
    const {translationError,showTranslation,signArray,savingTranslation} = useSelector(state => state.translationReducer)
    const localhost = "http://localhost:3000/individial_signs/"

    /**
     * local state
     */
    const [translationState, setTranslationState] = useState({
        translationText: "",
        signArray: [],
        showTranslation: false,
        translationError: "",
        savingTranslation: false
    })

    /**
     * saves the users text input in local state
     */
    const onInputChange = event => {
        setTranslationState({
            ...translationState,
            [event.target.id]: event.target.value
        })
    }

    /**
     *translates the text when the form is submitted
     */
    const onFormSubmit = event => {
        event.preventDefault();
        let text ="";
        text += translationState.translationText;
        text = text.toLowerCase();
        let array = [];
        for (let i = 0;i <text.length;i++) {
            array.push(text[i]);
        }
        console.log(array);
        let signs = [];
        array.forEach(char => {
            signs.push(findSign(char));
        })
        const { id } = JSON.parse(localStorage.getItem('slt-ss'))
        let translation = {
            text: translationState.translationText,
            signs: signs,
            status: "",
            user: id
        }
        dispath(translationSaveAction(translation))
    }

    /**
     *returns the src for the correct sign for the char
     */
    const findSign = (char) => {
        switch (char) {
            case "a":
                return localhost + "a.png"
            case "b":
                return localhost + "b.png"
            case "c":
                return localhost + "c.png"
            case "d":
                return localhost + "d.png"
            case "e":
                return localhost + "e.png"
            case "f":
                return localhost + "f.png"
            case "g":
                return localhost + "g.png"
            case "h":
                return localhost + "h.png"
            case "i":
                return localhost + "i.png"
            case "j":
                return localhost + "j.png"
            case "k":
                return localhost + "k.png"
            case "l":
                return localhost + "l.png"
            case "m":
                return localhost + "m.png"
            case "n":
                return localhost + "n.png"
            case "o":
                return localhost + "o.png"
            case "p":
                return localhost + "p.png"
            case "q":
                return localhost + "q.png"
            case "r":
                return localhost + "r.png"
            case "s":
                return localhost + "s.png"
            case "t":
                return localhost + "t.png"
            case "u":
                return localhost + "v.png"
            case "w":
                return localhost + "w.png"
            case "x":
                return localhost + "x.png"
            case "y":
                return localhost + "y.png"
            case "z":
                return localhost + "z.png"
            default:
                return ""
        }
    }

    return (
        <main>
            { !loggedIn && <Redirect to={"/"}/>}
            { loggedIn &&
            <AppContainer>

            <h1 className={"text-white"}>Translator</h1>
                <label htmlFor="translationText" className={"form-label text-white"}>What do you want to translate</label>
                <form className="input-group" onSubmit={onFormSubmit}>
                    <input
                        id={"translationText"}
                        type="text"
                        placeholder={"Hello"}
                        className={"form-control"}
                        onChange={onInputChange}
                    />
                    <button type={"submit"} className={"btn btn-primary btn-lg"}>Translate</button>
                </form>

                {showTranslation &&
                    <div className={"translation container bg-light rounded mt-3"}>
                        {signArray.map((sign,index) => <img className={"Sign_Image"} key={sign + index} src={sign} alt={sign} />)}
                        <h4>Translation</h4>
                    </div>


                }
                {!showTranslation &&
                    <div className={"translation container bg-light rounded mt-3"}>
                        <br/>
                        <p>Translation</p>
                    </div>
                }

                { savingTranslation &&
                <p className={"text-white"}>Saving translation...</p>
                }
                { translationError &&
                <div className={"alert alert-danger"} role={"alert"}>
                    <h4 className={"text-white"}>Unsuccessful</h4>
                    <p className={"mb-0 text-white"}>{translationError}</p>
                </div>
                }
                <Link to={"/profile"}>Profile</Link>
            </AppContainer>
            }


        </main>
    )
}
export default Translator;