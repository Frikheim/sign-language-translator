export const TranslatorAPI = {
    saveTranslation(translation){
        return fetch(`http://localhost:8080/history`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(translation)
        }).then(async response => {
            if (!response.ok){
                const { error = 'An unknown error occurred' } = await response.json()
                throw new Error(error)
            }
            return response.json()
        })
    }
}