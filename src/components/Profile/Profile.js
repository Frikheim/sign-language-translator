import {useEffect, useState} from "react";
import {ProfileAPI} from "./ProfileAPI";
import AppContainer from "../../hoc/AppContainer";
import {Link, Redirect} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {sessionLogoutAction} from "../../store/actions/sessionActions";

const Profile = () => {

    const dispatch = useDispatch();
    const {loggedIn} = useSelector(state => state.sessionReducer);
    const [state, setState] = useState({
        userHistory: [],
        latestHistory: [],
        fetching: true,
        error: ""
    })

    /**
     * function for when the user clicks on the clear button
     * sets the status to deleted on all the translations and updates them in the database
     */
    const clearClick = () => {
        state.userHistory.forEach(translation => {
            translation.status = "deleted";
            ProfileAPI.delete(translation)
                .catch(error => {
                    setState({
                        ...state,
                        error: error.message,
                        fetching: false
                    })
                })
        })
        setState({
            ...state,
            userHistory: [],
            latestHistory: []
        })
    }

    /**
     * function for when the user clicks on the logout button
     * deletes the session and redirects to loginpage
     */
    const logOutClick = () => {
        localStorage.removeItem('slt-ss');
        dispatch(sessionLogoutAction());
    }

    const session = JSON.parse(localStorage.getItem('slt-ss'))

    /**
     * fetches the translations from the database
     * creates a list of the users translations and the 10 most recent
     */
    useEffect(() => {
        ProfileAPI.getHistory()
            .then(history => {
                const { id } = JSON.parse(localStorage.getItem('slt-ss'));
                let userHistory = [];
                history.forEach(translation=> {
                    if (translation.user === id) {
                        if (!translation.status) {
                            userHistory.push(translation);
                        }

                    }
                })
                if (userHistory.length > 10) {
                    userHistory = userHistory.slice(userHistory.length - 10);
                }
                setState({
                    ...state,
                    error: "",
                    fetching: false,
                    history: history,
                    latestHistory: userHistory
                })
            })
            .catch(error => {
                setState({
                    ...state,
                    error: error.message,
                    fetching: false
                })
            })
    }, [])
    return (
        <main>
            { !loggedIn && <Redirect to={"/"}/>}
            {loggedIn &&
            <AppContainer>
                <h2 className={"text-white"}>{session.name}'s history</h2>
                {state.fetching && <p className={"text-white"}>History is loading...</p>}
                {!state.fetching &&

                <AppContainer>
                    <button onClick={clearClick} className={"btn btn-danger btn-lg"}>Clear history</button>
                    <button onClick={logOutClick} className={"btn btn-warning btn-lg"}>Logout</button>
                    <ul>
                        {state.latestHistory.map(translation => <li className={"text-white"} key={translation.id}>
                            <p className={"text-white"}>Translation of {translation.text}:</p>
                            <div className={"container bg-light rounded"}>
                            {translation.signs.map((sign, index) => <img className={"Sign_Image"} key={sign + index}
                                                                         src={sign} alt={sign}/>)}
                            </div>
                        </li>)}
                    </ul>
                </AppContainer>

                }
                {state.error&& <p>{state.error}</p>}
                <Link to={"/translator"}>Translator</Link>
            </AppContainer>
            }
        </main>
    )
}
export default Profile