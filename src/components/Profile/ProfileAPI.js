export const ProfileAPI = {
    getHistory() {

        return fetch(`http://localhost:8080/history`, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(async response => {
            if (!response.ok){
                const { error = 'An unknown error occurred' } = await response.json()
                throw new Error(error)
            }
            return response.json()
        })
    },
    delete(translation) {
        console.log("delete",translation)
        return fetch(`http://localhost:8080/history/${translation.id}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "text": translation.text,
                "signs": translation.signs,
                "status": "deleted",
                "user": translation.user
            })
        }).then(async response => {
            if (!response.ok){
                const { error = 'An unknown error occurred' } = await response.json()
                throw new Error(error)
            }
            return response.json()
        })
    }
}