import AppContainer from "../../hoc/AppContainer";
import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {loginAttemptAction} from "../../store/actions/loginActions";
import {Redirect} from "react-router-dom";

const Login = () => {

    const dispath = useDispatch();
    const {loginError, loginAttempting} = useSelector(state => state.loginReducer);
    const {loggedIn} = useSelector(state => state.sessionReducer);
    /**
     * local state
     */
    const [name, setName] = useState({
        name: ""
    })

    /**
     * saves the users name input in local state
     */
    const onInputChange = event => {
        setName({
            ...name,
            [event.target.id]: event.target.value
        })
    }

    /**
     *dispatches the name to the login action
     */
    const onFormSubmit = event => {
        event.preventDefault();
        dispath(loginAttemptAction(name))
    }

    /**
     * Login page. Form for inputing name and login
     */
    return (
        <>
            { loggedIn && <Redirect to={"/translator"}/>}
            { !loggedIn &&
                <AppContainer>
                    <AppContainer>
                        <p className={"fs-1"}><img className={"w-25"} src={"http://localhost:3000/logo.png"} alt={"page logo"}/> Get Started</p>
                    </AppContainer>

                    <AppContainer>
                        <label htmlFor="name" className={"form-label text-white"}>Name</label>
                        <form className="input-group" onSubmit={onFormSubmit}>
                            <input
                                id={"name"}
                                type="text"
                                placeholder={"What's your name?"}
                                className={"form-control"}
                                onChange={onInputChange}
                            />
                            <button type={"submit"} className={"btn btn-primary btn-lg"}>Login</button>
                        </form>
                    </AppContainer>



                    { loginAttempting &&
                        <p className={"text-white"}>Trying to login...</p>
                    }
                    { loginError &&
                        <div className={"alert alert-danger"} role={"alert"}>
                            <h4 className={"text-white"}>Unsucsessful</h4>
                            <p className={"mb-0 text-white"}>{loginError}</p>
                        </div>
                    }
                </AppContainer>
            }
        </>

    )
}

export default Login