export const LoginAPI = {
    login() {
        //use 8000 on json server
        return fetch("http://localhost:8080/names"//,{
            //method: "POST",
            //headers: {
             //   "Content-Type": "application/json"
            //},
            //body: JSON.stringify(name)
        //json}
        )
            .then( async (response) => {
                if (!response.ok) {
                    const {error = "Unknown error"} = await response.json();
                    throw new Error(error);
                }
                return response.json();
            })
    },
    newUser(user) {
        //use 8000 on json server
        return fetch("http://localhost:8080/names",{
            method: "POST",
            headers: {
               "Content-Type": "application/json"
            },
            body: JSON.stringify(user)
        })
            .then( async (response) => {
                if (!response.ok) {
                    const {error = "Unknown error"} = await response.json();
                    throw new Error(error);
                }
                return response.json();
            })
    }
}