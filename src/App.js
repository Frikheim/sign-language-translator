import './App.css';
import {
    BrowserRouter,
    Switch,
    Route
} from 'react-router-dom'
import Login from './components/Login/Login'
import Translator from "./components/Translator/Translator";
import Profile from "./components/Profile/Profile";

function App() {
  return (
      <BrowserRouter>
          <div className="App container border-bottom mb-3">
              <h3 className={"text-white"}>Sign Language Translator</h3>
          </div>

          <Switch>
              <Route path="/" exact component={Login}/>
              <Route path={"/translator"} component={Translator}/>
              <Route path={"/profile"} component={Profile}/>
          </Switch>
      </BrowserRouter>
  );
}

export default App;
